from script_mlp import run_model as run_smlp
from script_mlp_dropout import run_model as run_smlp_reg
from script_cnn import run_model as run_cnn
from script_cnn_reg import run_model as run_cnn_reg
from pathlib import Path
import pickle
# red a considerar, MLP standar
import numpy as np
import sys
import torch
from torch.utils.data import DataLoader
from torch.utils.data import random_split
import torchvision
from torchvision import transforms

# transform to apply to images
transform =  transforms.Compose(
  [transforms.ToTensor(), transforms.Normalize((0.5,),(0.5,))]
)

# load dataset train and test
trainset = torchvision.datasets.FashionMNIST(
    root='./data', 
    train=True, 
    download=True,  
    transform=transform)


evalset = torchvision.datasets.FashionMNIST(
    root='./data', 
    train=False,
    download=True, 
    transform=transform)
    

val_percent = 0.5
val_len = int(len(evalset)*val_percent)
valset, testset = random_split(
    evalset, [val_len, len(evalset)-val_len])


BATCH_SIZE = 128

train_loader = DataLoader(trainset, batch_size=BATCH_SIZE, shuffle=True)
valid_loader = DataLoader(valset, batch_size=BATCH_SIZE, shuffle=True)
test_loader = DataLoader(testset, batch_size=BATCH_SIZE, shuffle=True)


if __name__ == '__main__':
    model = sys.argv[1]
    if model.upper() == "MLP":
        run_smlp(valid_loader, train_loader)
    elif model.upper() == "MLP_DROPOUT":
        run_smlp_reg(valid_loader, train_loader)
    elif model.upper() == "CNN":
        run_cnn(valid_loader, train_loader)
    elif model.upper() == 'CNN_REG':
        run_cnn_reg(valid_loader, train_loader)
