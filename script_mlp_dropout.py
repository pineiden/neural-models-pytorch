from pathlib import Path
import pickle
# red a considerar, MLP standar
import numpy as np
import torch
#from torch.utils.data import DataLoader
from torch.utils.data import random_split
import torchvision
#from torchvision import transforms

from model_nn import MLP
from train_net import fit_net

def run_model(valid_loader, train_loader):
    # create dataloader
    # BATCH_SIZE = 128
    lr = 0.001
    betas = (0.9,0.999)
    epochs = 25

    DEVICE = 'cuda' if torch . cuda . is_available () else 'cpu'

    # obtener tamaño de la imagend
    images, labels = next(iter(train_loader))
    depth, rows, cols = images[0].shape
    first = rows * cols
    # definir dos capas para la red
    # la primera es definida por el tamaño de la entrada
    print("First layer", first)
    layers = [first,32]
    clases =  10
    criterion = torch.nn.CrossEntropyLoss()

    # sin dropout
    net = MLP(n_classes=clases, layers=layers, dropout=0.3)
    optimizer = torch.optim.Adam ( 
        net.parameters () , lr = lr , betas=betas)
    namepath = "mlp_dropout.model"

    return fit_net(
        valid_loader, 
        train_loader,
        criterion,# loss criteria cross correlation
        optimizer,
        net, # red entrenada
        epochs=epochs, 
        run_in_GPU=False if DEVICE == 'cpu' else True, 
        namepath=namepath)
