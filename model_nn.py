from torch import nn
from typing import Tuple, Dict, List, Any, Optional, Union, get_args, get_origin
from torch.nn import functional as F
from rich import print

class MLP(nn.Module):
    """
    Multilayer perceptrón

    """

    def __init__(
            self, 
            n_classes: int=10,
            layers:List[int]=[100,150, 80],
            dropout:float=0.0,
            **opts):
        super().__init__()
        # un acumulador ordenado de operaciones
        self.steps:nn.ModuleList = []
        # elemento regularizador para MLP
        self.dropout:nn.Dropout = None
        # asociar número de clases
        self.n_classes = n_classes
        # almacenar las opciones extra a la clase
        self.opts = opts
        if dropout:
            self.dropout = nn.Dropout(dropout) if dropout > 0.0 else None
        # metodo para crear las capas
        self.create_layers(layers)

    def create_layers(self, layers):
        # torch.nn.Linear(in_features, out_features, bias=True,
        # device=None, dtype=None)
        # se añade elemento de salida según cantidad de clases
        layers.append(self.n_classes)
        # se itera de manera ordenada por las capas
        # se crea cada nivel según la cantidad definida
        steps = []
        for i,nlayer in enumerate(layers[:-1]):
            layer = nn.Linear(
                nlayer, 
                layers[i+1], 
                bias=True) 
            # se añade capa al acumulador
            steps.append(layer)
        self.steps = nn.ModuleList(steps)

    def forward(self, x):
        """
        Se define la función que opera la entrada paso a paso y
        retorna una salida como resultado.
        """
        x_in = x.view(x.shape[0],-1)
        for layer in self.steps[:-1]:
            # se opera cada capa con la entrada y lo que retorna se
            # opera en la siguiente
            x_in = layer(x_in)
            frelu = F.relu(x_in)
            if self.dropout:
                # en caso de tene activado dropout...
                x_in = self.dropout(frelu)
            else:
                x_in = frelu
        last_layer = self.steps[-1]
        # se aplica log-softmax  a la salida
        ypred = F.log_softmax(last_layer(x_in), dim=1)
        #ypred = F.sigmoid(last_layer(x_in))
        return ypred


# se define un tipo Pools para agrupar posibles tipos de pool
Pools = Union[nn.MaxPool2d, nn.AvgPool2d]
# se define un tipo que agrupo las posibles etapas de una red convolucional
TorchStep = nn.Module

from enum import Enum
# se define enum para construir al capa segun configuracion
class ConvStep(Enum):
    CONV = 0
    POOL_AVG = 1
    POOL_MAX = 2

# se agrupan los valores mas tipicos
Values = Union[bool, int, float, str]
# Se define un tipo que define las settings para etapas de convolución
ConvOpts = Tuple[ConvStep,  Dict[str, Values]]

class CNNBase(nn.Module):
    """
    Convolutional Neural Network
    """

    def __init__(
            self, 
            n_classes: int=10,
            convolutions: List[ConvOpts]=[],
            layers:List[int]=[100,150,80], 
            **opts):
        super().__init__()
        
        self.modules:List[TorchStep] = []
        self.n_layers:List[nn.Linear] = []
        self.opts_convolutions:List[TorchStep] = []
        self.n_classes = n_classes
        # crea el bloque de convoluciones
        self.create_convolutions(convolutions)
        # crea el bloque de capa de red neutornal
        self.create_layers(layers)
        self.opts = opts
        # this go on the optimization
        self.steps = nn.ModuleList(self.modules)
        

    def create_convolutions(self, convolutions):
        # itera sobre la lista de presets y crea los objetos
        # acumulandolos en la lista de 'steps'
        for step, opts in convolutions:
            if step == ConvStep.CONV:
                conv = nn.Conv2d(**opts)
            elif step == ConvStep.POOL_AVG:
                conv = nn.AvgPool2d(**opts)
            elif step == ConvStep.POOL_MAX:
                conv = nn.MaxPool2d(**opts)
            self.opts_convolutions.append(conv)
        # se asocia la lista a los steps
        self.modules += self.opts_convolutions
        self.convert = len(self.modules) - 1


    def create_layers(self, layers):
        # torch.nn.Linear(in_features, out_features, bias=True,
        # device=None, dtype=None)
        # se añade elemento de salida según cantidad de clases
        layers.append(self.n_classes)
        # se itera de manera ordenada por las capas
        # se crea cada nivel según la cantidad definida
        steps = []
        for i,nlayer in enumerate(layers[:-1]):
            layer = nn.Linear(
                nlayer, 
                layers[i+1], 
                bias=True) 
            # se añade capa al acumulador
            steps.append(layer)
        self.modules += steps

  
    def forward(self, x):
        """
        Opera la entrada por cada etapa de 'steps'
        Primero por el bloque convulcional, las pools
        y luego, finalmente, por las capas lineales

        opcionalmente, se puede implementar Sequential, con nn.ReLu
        """
        x_in = x
        # se itera por cada capa, bloque a bloque hasta antes del final
        for i, layer in enumerate(self.steps[:-1]):
            #print(f"In layer-> {i}",x_in.shape)
            if isinstance(layer, get_args(Pools)):
                #print("Doing pools")
                x_in = layer(x_in)
            else:
                x_in = layer(x_in)
                x_in = F.relu(x_in)
            #print(i, self.convert, i == self.convert)
            if i == self.convert:
                #print("Reshaping")
                #print(f"PRE Oout layer {i} ->")
                #print(x_in.shape)
                x_in = x_in.view(x_in.shape[0], -1)
            #print(f"Oout layer {i} ->")
            #print(x_in.shape)

        last_layer = self.steps[-1]
        # se aplica a la salida la log-softmax
        ypred = F.log_softmax(last_layer(x_in), dim=1)
        return ypred

from layer_size import conv_size, max_pool_size

class CNN(CNNBase):
    """
    Se crea una subclase que configura una red convolucional
    específica según lo que expresa la guía de desarrollo.
    """
    def __init__(
            self, 
            in_channels=3,
            **opts):
        convolutions = [
           (ConvStep.CONV, {
               "in_channels":in_channels,
               "out_channels": 128,
               "kernel_size": 3,
               "stride":1,
               "padding":1
           }), # conv
           (ConvStep.POOL_MAX,{
               "kernel_size":2,
               "stride":1,
               "padding":1
           }), # pool
           (ConvStep.CONV,{
               "in_channels":128,
               "out_channels":256,
               "kernel_size": 3,
               "stride":1,
               "padding":1
           }), # conv
           (ConvStep.POOL_MAX,{
               "kernel_size":2,
               "stride":2,
               "padding":0
           })  # pool
        ]
        layers = [50176, 2048, 64]
        clases = 10
        super().__init__(clases, convolutions, layers)
