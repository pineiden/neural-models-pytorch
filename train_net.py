from torch.optim import Optimizer
from torch import nn
from torch.utils.data import DataLoader
import time
import sys
import torch
from rich import print
import numpy as np
from pathlib import Path
import pickle
from datetime import datetime
import copy

def eval_net(net, dataset, device, perdida):
    # No necesitamos crear el grafo computacional
    with torch.no_grad():
        lloss = 0.0
        correct = 0
        total = 0
        predictions_list = []
        target_list = []
        for x,y in dataset:        
            X, Y = x.to(device), y.to(device)  
            target_list += Y
            # Predecimos con la red
            Y_PRED = net(X)
            # Calculamos la loss en entrenamiento
            predictions =torch.max(Y_PRED,1)[1].to(device)
            
            correct += (predictions == Y).sum()
            total += len(y)
            loss = perdida(Y_PRED, Y)  
            lloss += (loss.item()*X.size(0))
            #acc.append(accuracy)
            predictions_list += predictions
        acc = correct *  100 /total
        return lloss/len(dataset.sampler), acc, predictions_list, target_list


def fit_net(
    test_loader:DataLoader, 
    dataloader:DataLoader,
    criterion: nn.CrossEntropyLoss,
    optimizer: Optimizer,
    net:nn.Module, 
    epochs:int, 
    run_in_GPU:bool=True, 
    reports_every:int=1, 
    cheq_grad:bool=False, 
    namepath:str="MLP.model"):
    # activar uso de GPU
    device  = 'cuda' if run_in_GPU else 'cpu'
    # conectar red a dispositivo
    net.to(device)
    # activate to train
    net.train()
    # mostrar red
    print("Red", net)
    tiempo_epochs = 0
    train_losses = []
    valid_losses = []
    demora = []
    accuracy = []
    datalosses = {}
    start = datetime.utcnow()
    now = None
    the_best = None
    before = 0
    pos = 0

    for e in range(1, epochs+1):
        running_loss = 0.0
        inicio_epoch= time.perf_counter()
        for (images,labels) in dataloader:
            # optmizador a 0, limpiiando gradientes
            optimizer.zero_grad()            
            # Aseguramos de pasarlos al dispositivo elegido
            images, labels = images.to(device), labels.to(device)

            # Aplicamos la pasada hacia adelante de la red
            # net is a 'callable', ejecuta forward al usarla como fn
            labels_pred = net(images)

            loss = criterion(labels_pred,labels)
            running_loss += loss.item()*images.size(0)
            # Calculamos los gradientes (backpropagation)
            loss.backward()

            # Actualizamos los parámetros con descenso del gradiente3
            optimizer.step()

            # Dejamos en 0 a los gradientes
            optimizer.zero_grad()    

            del images
            del labels
        delta = time.perf_counter() - inicio_epoch
        demora.append(delta)
        tiempo_epochs += delta
        rl = running_loss/len(dataloader.sampler)
        # Terminada la época, calculamos la loss, y el acurracy en train y test
        test_loss, acc, preds, targs = eval_net( net, test_loader, device, criterion)
        # salidatorch.cuda.memory_allocated(0)
        accuracy.append(acc)
        train_losses.append(rl)
        valid_losses.append(test_loss)
        now = datetime.utcnow()
        if acc > before:
            bk_net = net.to("cpu").state_dict()
            the_best = bk_net
            before = acc
            pos = e
        print(now)
        print(f"""
        Epoch:{e:03d}
        Train Loss: {rl}
        Test_Loss:{test_loss:.4f}
        Tiempo/epoch:{delta:0.3f}s
        Accuracy: {acc:0.3f}%
        """)
    total = None
    if now:
        total = (now - start).total_seconds()
    print(f"Duracion: {total}")
    print("Saving model")    
    path = Path(namepath)
    print(f"Best model at {pos}")
    print(the_best)
    if path.exists():
        path.unlink()
    if not path.exists():
        torch.save(net.to("cpu").state_dict(), namepath)

        if the_best:
            torch.save(the_best, f"{namepath}_best")

        path = Path(f"{namepath}_loss")

        datalosses = {
            "train":train_losses,
            "test": valid_losses,
            "acc": accuracy,
            "demora": demora
        }

        databytes = pickle.dumps(datalosses)
        path.write_bytes(databytes)

        return datalosses, the_best
    return datalosses, the_best
