from confusion import plot_confusion_matrix
from pathlib import Path
import torch
import pickle
# red a considerar, MLP standar
import numpy as np
import sys
import torch
from torch.utils.data import DataLoader
from torch.utils.data import random_split
import torchvision
from torchvision import transforms
from model_nn import CNN, MLP

# transform to apply to images
transform =  transforms.Compose(
  [transforms.ToTensor(), transforms.Normalize((0.5,),(0.5,))]
)

# load dataset train and test
trainset = torchvision.datasets.FashionMNIST(
    root='./data', 
    train=True, 
    download=True,  
    transform=transform)


evalset = torchvision.datasets.FashionMNIST(
    root='./data', 
    train=False,
    download=True, 
    transform=transform)
    

val_percent = 0.5
val_len = int(len(evalset)*val_percent)
valset, testset = random_split(
    evalset, [val_len, len(evalset)-val_len])


BATCH_SIZE = 128
clases = [
    "tshirt",
    "trouser",
    "pullover",
    "dress",
    "coat",
    "sandal",
    "shirt",
    "sneaker",
    "bag",
    "ankle boot"
]

train_loader = DataLoader(trainset, batch_size=BATCH_SIZE, shuffle=True)
valid_loader = DataLoader(valset, batch_size=BATCH_SIZE, shuffle=True)
test_loader = DataLoader(testset, batch_size=BATCH_SIZE, shuffle=True)


models = Path (__file__).parent / "models"

from train_net import eval_net

criterion = torch.nn.CrossEntropyLoss()
device = 'cuda' if torch . cuda . is_available () else 'cpu'
lr = 0.001
betas = (0.9,0.999)
from rich import print

images, labels = next(iter(train_loader))
depth, rows, cols = images[0].shape
first = rows * cols
layers = [first,32]


settings = {
   "mlp.model":{
       "n_classes":10,
       "layers": [784,32],
   }, 
   "mlp_dropout.model":{
       "n_classes":10,
       "layers": [784,32],
       "dropout": 0.3
   }, 
   "cnn.model":{
       "in_channels":1,
   }, 
   "cnn_reg.model":{
       "in_channels":1,
   }, 
}
for path in models.glob("*.model"):
    print(path)
    title = path.name.split(".")[0]
    if str(path.name).startswith("cnn"):
        opts = settings.get(path.name)
        model = CNN(**opts)
    elif str(path.name).startswith("mlp"):
        print("path->", path.name)
        opts = settings.get(path.name)
        print(opts)
        model = MLP(**opts)
        print(model.state_dict().keys())
    data = torch.load(path)
    print(data.keys())
    model.load_state_dict(data)
    model.to(device)

    #databytes = path.read_bytes()
    #model = pickle.loads(databytes)
    test_loss, acc, preds, targs = eval_net( 
        model, 
        test_loader, 
        device, 
        criterion)
    #print(preds)
    plot_confusion_matrix(
        preds, 
        targs, 
        f"{path.name}.png", clases, title)
