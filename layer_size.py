def conv_size(
        width, height, 
        n_kernels, 
        kernel_size=3, 
        stride=1, 
        padding=0):
    """
    Given image size
    """
    w = (width-kernel_size + 2 * padding) / stride + 1
    h = (height-kernel_size + 2 * padding) / stride + 1
    return (w,h)


def max_pool_size(
        width, 
        height, 
        stride, 
        pool_size):
    w = (width-pool_size) / stride
    h = (width-pool_size) / stride
    return (w, h)
