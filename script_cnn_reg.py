from pathlib import Path
import pickle
# red a considerar, MLP standar
import numpy as np
import torch
from torch.utils.data import DataLoader
from torch.utils.data import random_split
import torchvision
from torchvision import transforms

from model_nn import CNN
from train_net import fit_net
from rich import print



def run_model(valid_loader, train_loader):
    # usar cuda si está habilitado
    lr = 0.001
    betas = (0.9,0.999)
    epochs = 25

    DEVICE = 'cuda' if torch . cuda . is_available () else 'cpu'
    # obtener tamaño de la imagend
    images, labels = next(iter(train_loader))
    depth, rows, cols = images[0].shape
    in_channels =  depth
    first = rows * cols
    layers = [first,32]
    clases =  10
    reg = 0.001
    criterion = torch.nn.CrossEntropyLoss()
    net = CNN(in_channels=in_channels, device=DEVICE)
    optimizer = torch.optim.Adam ( 
        net.parameters () , 
        lr = lr , 
        betas=betas,
        weight_decay=reg) #for reg

    namepath = "cnn_reg.model"

    return fit_net(
        valid_loader, 
        train_loader,
        criterion,# loss criteria cross correlation
        optimizer,
        net, # red entrenada
        epochs=epochs, 
        run_in_GPU=True, 
        namepath=namepath)


