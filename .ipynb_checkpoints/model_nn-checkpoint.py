from torch import nn
from typing import Tuple, Dict, List, Any, Optional, Union, get_args, get_origin
from torch.nn import functional as F

class MLP(nn.Module):
    """
    Multilayer perceptrón

    """
    # un acumulador ordenado de operaciones
    layers:List[nn.Linear]
    # elemento regularizador para MLP
    dropout:nn.Dropout = None

    def __init__(
            self, 
            size:Tuple[int,int], 
            n_classes: int,
            layers:List[int]=[100,150, 80],
            dropout:float=0.3,
            **opts):
        super().__init__()
        # asociar número de clases
        self.n_classes = n_classes
        # almacenar las opciones extra a la clase
        self.opts = opts
        if dropout > 0:
            # si es que dropuot es mayor a 0 se crea un objeto regulador
            self.dropout = nn.Dropout(dropout)
        # metodo para crear las capas
        self.create_layers(layers)

    def create_layers(self, layers):
        # torch.nn.Linear(in_features, out_features, bias=True,
        # device=None, dtype=None)
        # se añade elemento de salida según cantidad de clases
        layers.append(self.n_classes)
        # se itera de manera ordenada por las capas
        # se crea cada nivel según la cantidad definida
        for i,nlayer in enumerate(layers):
            layer = nn.Linear(nlayer, layers[i+1], bias=True) 
            # se añade capa al acumulador
            self.layers.append(layer)
    
    def forward(self, x):
        """
        Se define la función que opera la entrada paso a paso y
        retorna una salida como resultado.
        """
        x_in = x
        for layer in self.layers[:-1]:
            # se opera cada capa con la entrada y lo que retorna se
            # opera en la siguiente
            x_in = layer(x_in)
            frelu = F.relu(x_in)
            if self.dropout:
                # en caso de tene activado dropout...
                x_in = self.dropout(frelu)
            else:
                x_in = frelu
        last_layer = self.layers[-1]
        # se aplica log-softmax  a la salida
        ypred = F.log_softmax(last_layer(x_in), dim=1)
        return ypred


# se define un tipo Pools para agrupar posibles tipos de pool
Pools = Union[nn.MaxPool2d, nn.AvgPool2d]
# se detine un tipo que agrupo las posibles etapas de una red convolucional
TorchStep = Union[Pools, nn.Conv2d, nn.Linear]

from enum import Enum
# se define enum para construir al capa segun configuracion
class ConvStep(Enum):
    CONV = 0
    POOL_AVG = 1
    POOL_MAX = 2

# se agrupan los valores mas tipicos
Values = Union[bool, int, float, str]
# Se define un tipo que define las settings para etapas de convolución
ConvOpts = Tuple[ConvStep, int, Dict[str, Values]]

class CNNBase(nn.Module):
    """
    Convolutional Neural Network
    """
    steps:List[TorchStep]
    layers:List[nn.Linear]
    convolutions:List[TochStep]

    def __init__(
            self, 
            size:Tuple[int,int], 
            convolutions: List[ConvOpts],
            layers:List[int]=[100,150,80], 
            in_channels=3, 
            regularization=0.001,
            **opts):
        super().__init__()
        # crea el bloque de convoluciones
        self.create_convolutions(convolutions)
        # crea el bloque de capa de red neutornal
        self.create_layers(layers)
        self.opts = opts
        # this go on the optimization
        self.weigth_decay = regularization
        

    def create_convolutions(self, convolutions):
        # itera sobre la lista de presets y crea los objetos
        # acumulandolos en la lista de 'steps'
        for conv, opts in convolutions:
            if conv == ConvOpts.CONV:
                conv = torch.nn.Conv2d(**opts)
            elif conv == ConvOpts.POOL_AVG:
                pool = nn.AvgPool2d(**opts)
            elif conv == ConvOpts.POOL_MAX:
                pool = nn.MaxPool2d(**opts)
            self.convolutions.append(conv)
        # se asocia la lista a los steps
        self.steps += self.convolutions
    
    def create_layers(self, layers):
        # crea las capas iterando sobre los presets de 'layers'
        layers.append(self.n_classes)
        for i,nlayer in enumerate(layers):
            layer = torch.nn.Linear(nlayer, layers[i+1], bias=True) 
            self.layers.append(layer)
        # se asocia lista de layers a los steps
        self.steps += self.layers
    
    def forward(self, x):
        """
        Opera la entrada por cada etapa de 'steps'
        Primero por el bloque convulcional, las pools
        y luego, finalmente, por las capas lineales

        opcionalmente, se puede implementar Sequential, con nn.ReLu
        """
        x_in = x
        # se itera por cada capa, bloque a bloque hasta antes del final
        for layer in self.layers[:-1]:
            if isinstance(layer, get_args(Pools)):
                x_in = layer(x_in)
            else:
                x_in = layer(x_in)
                x_in = F.relu(x_in)
        last_layer = self.layers[-1]
        # se aplica a la salida la log-softmax
        ypred = F.log_softmax(last_layer(x_in), dim=1)
        return ypred

from layer_size import conv_size, max_pool_size

class CNN(CNNBase):
    """
    Se crea una subclase que configura una red convolucional
    específica según lo que expresa la guía de desarrollo.
    """
    def __init__(
            self, 
            size:Tuple[int,int], 
            in_channels=3
            **opts):
        convolutions = [
           (ConvStep.CONV, {
               "in_channels":in_channels,
               "out_channels": 96,
               "kernel_size": 5,
               "stride":1,
               "padding":1
           }), # conv
           (ConvStep.POOL_MAX,{
               "kernel_size":2,
               "stride":2,
               "padding":0
           }), # pool
           (ConvStep.CONV,{
               "in_channels":256,
               "kernel_size": 3,
               "stride":1,
               "padding":1
           }), # conv
           (ConvStep.POOL_MAX,{
               "kernel_size":2,
               "stride":2,
               "padding":0
           })  # pool
        ]
        layers = [10]
        super().__init__(size, convolutions, layers)
