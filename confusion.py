from sklearn.metrics import confusion_matrix,ConfusionMatrixDisplay
import matplotlib.pyplot as plt

def plot_confusion_matrix(predicciones, testset_target, name, clases, title):

    preds = [p.to("cpu") for p in predicciones]
    targs = [t.to("cpu") for t in testset_target]
    cm = confusion_matrix(
        targs,preds,
        labels = [i for i in range(10)])

    plt.rcParams.update({'font.size': 12})
    plt.rc('axes', labelsize=15)
    fig, ax = plt.subplots(figsize=(15, 10))
    disp = ConfusionMatrixDisplay(
        confusion_matrix=cm,
        display_labels=clases)
    disp.plot(ax = ax,
              cmap = plt.cm.Blues)
    plt.title(f'Confusion Matrix {title}')
    plt.savefig(name)
