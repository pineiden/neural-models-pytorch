from pathlib import Path
import pickle
import torch


models = Path(__file__).parent
datasets = {}
results = Path("all_results_on_cpu.model")

for path in models.rglob("*.model_loss"):
    databytes = path.read_bytes()#torch.load(str(path),
                #           map_location=torch.device('cpu'))
    code = path.name.split(".")[0]
    data = pickle.loads(databytes)
    new_data = {}
    for elem, itemset in data.items():
        itemset = [float(e) for e in itemset]
        new_data[elem] = itemset
    datasets[code] = new_data

databytes = pickle.dumps(datasets)
results.write_bytes(databytes)
