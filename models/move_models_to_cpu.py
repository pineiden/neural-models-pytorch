from pathlib import Path
import pickle
import torch

models = Path(__file__).parent
datasets = {}
results = Path("all_models_results_on_cpu.model_loss")

for path in models.rglob("*.model*"):
    if not str(path).endswith("loss"):
        databytes = torch.load(str(path), map_location=torch.device('cpu'))
        code = path.name
        datasets[code] = databytes

print(datasets.keys())
databytes = pickle.dumps(datasets)
results.write_bytes(databytes)
